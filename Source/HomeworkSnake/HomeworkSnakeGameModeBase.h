// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeworkSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORKSNAKE_API AHomeworkSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
